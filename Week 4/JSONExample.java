package Week04;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class JSONExample {

    public static String wowClassToJSON (WoWClass wowClass) {
        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(wowClass);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }

    public static WoWClass JSONToWowClass(String s) {
        ObjectMapper mapper = new ObjectMapper();
        WoWClass wClass = null;

        try {
            wClass = mapper.readValue(s, WoWClass.class);
        } catch  (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return wClass;

    }

    public static void main(String[] args) {
        WoWClass wClass1 = new WoWClass();
        wClass1.setWowClass("Warrior");
        wClass1.setSpec("Fury");

        String json = JSONExample.wowClassToJSON(wClass1);
        System.out.println(json);

        WoWClass wClass2 = JSONExample.JSONToWowClass(json);
        System.out.println(wClass2);
    }
}