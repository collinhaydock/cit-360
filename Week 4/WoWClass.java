package Week04;

public class WoWClass {
    private String wowClass;
    private String spec;

    public String getWowClass() {
        return wowClass;
    }
    public void setWowClass(String wowClass) {
        this.wowClass = wowClass;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String toString() {
        return "I play a  " + spec + wowClass;
    }
}
