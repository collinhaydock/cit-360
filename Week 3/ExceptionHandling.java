package Week03;
import java.util.Scanner;
public class ExceptionHandling {
    public static void main(String args[]) {
        Scanner number = new Scanner(System.in);
        int quotient = 0;

        System.out.println("Enter number 1");
        int num1 = number.nextInt();

        System.out.println("Enter Number 2");
        int num2 = number.nextInt();

        try {
            quotient = divide(num1, num2);
        } catch (ArithmeticException e) {
            while (num2 == 0) {
            System.out.println("Please enter a number that is not 0 for the second number.");
            num2 = number.nextInt();
            }
            quotient = divide(num1, num2);
        } finally {
            System.out.println("The quotient is " + quotient);
        }
    }

    public static int divide(int num1, int num2) throws ArithmeticException {
        int quotient = num1 / num2;
        return quotient;
    }

}
