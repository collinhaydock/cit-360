package Week03;
import java.util.Scanner;
public class DataValidation {
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        int quotient = 0;

        System.out.println("Enter number 1"); //Enter the first number
        int num1 = input.nextInt();

        System.out.println("Enter Number 2"); //Enter the second number.
        int num2 = input.nextInt();

            quotient = divide(num1, num2);    //Call divide to get the quotient.

            while (num2 == 0) {               //While the user puts 0 as the denominator ask them for a number that isn't 0
                System.out.println("Please enter a number that is not 0 for the second number.");
                num2 = input.nextInt();
            }
            System.out.println("The quotient is " + quotient); //Display the quotient.
    }

    public static int divide(int num1, int num2) throws ArithmeticException {
        int quotient = num1 / num2;
        return quotient;
    }

}