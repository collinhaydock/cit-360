package edu.haydock;

import java.util.*;

public class CollectionTypes {

    public static void main(String[] args) {
        System.out.println("-- List --");
        List list = new ArrayList();
        list.add("Hi");
        list.add("I'm");
        list.add("learning");
        list.add("how");
        list.add("to");
        list.add("code");
        list.add("Hi");

        for (Object str : list) {
            System.out.println((String)str);
        }

        System.out.println("-- Set --");
        Set set = new TreeSet();
        set.add("Hi");
        set.add("I'm");
        set.add("learning");
        set.add("how");
        set.add("to");
        set.add("code");
        set.add("Hi");

        for (Object str : set) {
            System.out.println((String)str);
        }
        System.out.println("-- Queue --");
        Queue queue = new PriorityQueue();
        queue.add("Hi");
        queue.add("I'm");
        queue.add("learning");
        queue.add("how");
        queue.add("to");
        queue.add("code");
        queue.add("Hi");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

    }

}
